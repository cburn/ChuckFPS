using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMovement : MonoBehaviour {
  public CharacterController characterController;
  public float regularSpeed = 10.0f;

  public float gravity = -20f;
  public Vector3 velocity = Vector3.zero;

  public bool isGrounded;
  public Transform groundCheck;
  public float groundDist;
  public LayerMask groundMask;

  // Start is called before the first frame update
  void Start() {}

  // Update is called once per frame
  void Update() {
    isGrounded =
        Physics.CheckSphere(groundCheck.position, groundDist, groundMask);

    Debug.Log(velocity);
    /* resetVelocity(); */
    if (isGrounded && velocity.y < 0) {
      velocity.y = -2f;
    }
    float x = Input.GetAxis("Horizontal");
    float z = Input.GetAxis("Vertical");

    Vector3 movement = transform.right * x + transform.forward * z;
    Vector3 outputMovement = movement * regularSpeed * Time.deltaTime;
    characterController.Move(outputMovement);

    velocity.y += gravity * Time.deltaTime;
    if (! isGrounded) Debug.Log(velocity.y);
    characterController.Move(velocity * Time.deltaTime);
  }
  void resetVelocity() {
    if (isGrounded && velocity.y < 0) {
      velocity.y = -2f;
    }
  }
}
