using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    // Start is called before the first frame update
    public float percentChanceRoam;
    private List<GameObject> players;
    private List<GameObject> roamingPoints;
    private GameObject objective;
    private ZombieStateEnum.State zombieState;

    void Start()
    {
        zombieState = ZombieStateEnum.State.Idle;

    }

    // Update is called once per frame
    void Update()
    {
        if(zombieState == ZombieStateEnum.State.Idle){

        }
        else if (zombieState == ZombieStateEnum.State.Roaming){

        }
        else if (zombieState == ZombieStateEnum.State.Activated){

        }
    }
}
