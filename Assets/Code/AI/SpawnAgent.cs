using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnAgent : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Agent;
    void Start()
    {
        MapGenerator[] mapGeneratorArr = GameObject.FindObjectsOfType<MapGenerator>();
        if (mapGeneratorArr.Length != 1) {
            Debug.LogError("More than one map generator detected");
        }
        MapGenerator mapGenerator = mapGeneratorArr[0];
        Debug.Log(mapGenerator.start);
        Agent = Instantiate(Agent);
        NavMeshAgent navMeshAgent = Agent.GetComponent<NavMeshAgent>();
        navMeshAgent.SetDestination(new Vector3(100, 0, 100));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
