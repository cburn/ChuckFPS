using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Administrator : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject MapGenerator;
    public GameObject AIManager;

    public GameObject PlayersManager;
    void Start()
    {
        /* MapGenerator.Start(); */
        MapGenerator = Instantiate(MapGenerator);
        MapGenerator.transform.parent = transform;
        AIManager = Instantiate(AIManager);
        AIManager.transform.parent = transform;
        PlayersManager = Instantiate(PlayersManager);
        PlayersManager.transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
