using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class MapGeneratorAbstract
{
    abstract public void setStart(Vector3 start);
    abstract public void setFinish(Vector3 finish);
    abstract public void setMaxDepth(int maxDepth);
    abstract public void setMinDepth(int minDepth);
    abstract public Vector3[] generatePath();
}
