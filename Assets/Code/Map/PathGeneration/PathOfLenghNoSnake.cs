using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathOfLengthNoSnake : MapGeneratorAbstract
{
    private Vector3 start;
    private Vector3 finish;
    private int maxDepth;
    private int minDepth;
    private List<Vector3> visited;

    public PathOfLengthNoSnake(Vector3 start, Vector3 finish, int minDepth, int maxDepth) {
        this.start = start;
        this.finish = finish;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }

    override public void setStart(Vector3 start){
        this.start = start;
    }
    override public void setFinish(Vector3 finish){
        this.finish = finish;
    }
    override public void setMaxDepth(int maxDepth){
        this.maxDepth = maxDepth;
    }
    override public void setMinDepth(int minDepth){
        this.minDepth = minDepth;
    }
    override public Vector3[] generatePath(){
        Stack<Vector3> path = new Stack<Vector3>();
        List<Vector3> visited = new List<Vector3>();
        path.Push(start);
        visited.Add(start);
        path = dfs_of_len(path);
        return path.ToArray();
    }

    private Stack<Vector3> dfs_of_len(Stack<Vector3> path){
        if (path.Count > minDepth) {
            return path;
        }
        Vector3 current = path.Peek();
        ArrayList options = getAdjacentV3s(current, path);
        while (options.Count > 0) {
            int r = Random.Range(0, options.Count);
            Vector3 chosen = (Vector3) options[r];
            path.Push(chosen);
            options.RemoveAt(r);
            path = dfs_of_len(path);
            if (path.Count > minDepth) {
                break;
            }
            path.Pop();
        }
        return path;
    }

    private ArrayList getAdjacentV3s(Vector3 v, Stack<Vector3>visited) {
        ArrayList adj = new ArrayList();
        Vector3 nv = new Vector3(v.x-1, v.y, v.z);
        Vector3 a1 = new Vector3(v.x-1, v.y, v.z-1);
        Vector3 a2 = new Vector3(v.x-1, v.y, v.z+1);
        if(!visited.Contains(nv) & !visited.Contains(a1) && !visited.Contains(a2)){
            adj.Add(nv);
        }
        nv = new Vector3(v.x+1, v.y, v.z);
        a1 = new Vector3(v.x+1, v.y, v.z-1);
        a2 = new Vector3(v.x+1, v.y, v.z+1);
        if(!visited.Contains(nv) & !visited.Contains(a1) && !visited.Contains(a2)){
            adj.Add(nv);
        }
        nv = new Vector3(v.x, v.y, v.z-1);
        a1 = new Vector3(v.x-1, v.y, v.z-1);
        a2 = new Vector3(v.x+1, v.y, v.z-1);
        if(!visited.Contains(nv) & !visited.Contains(a1) && !visited.Contains(a2)){
            adj.Add(nv);
        }
        nv = new Vector3(v.x, v.y, v.z+1);
        a1 = new Vector3(v.x-1, v.y, v.z+1);
        a2 = new Vector3(v.x+1, v.y, v.z+1);
        if(!visited.Contains(nv) & !visited.Contains(a1) && !visited.Contains(a2)){
            adj.Add(nv);
        }

        return adj;
    }
}
