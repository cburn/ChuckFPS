using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensitivityMenu : MonoBehaviour {
  // Start is called before the first frame update
  public bool isPaused = false;
  public GameObject Sensitivity;
  void Start() {}

  // Update is called once per frame
  void Update() {
    if (Input.GetKeyDown(KeyCode.Escape)) {
      isPaused = !isPaused;

      if (isPaused) {
        Sensitivity.SetActive(true);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
      } else {
        Sensitivity.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
      }
    }
  }
}
